#include <iostream>
#include<string>
#include<cmath>
using namespace std;
int integer=0;
int power=0;

class ExceptionClass
{
public:
    class OutRange{};
};
int convertToInt(const string& stri)
{
    for (int i=0 ; i< stri.size() ; i++)
        {
            if (stri[i]>'9' || stri[i]<'0')
                throw ExceptionClass::OutRange();
        }
    if(stri.size()==1)
        {
         integer +=(stri[0]-'0')*round(pow(10,power));
         return integer;
        }
    else
        {
            integer+=(stri[stri.size()-1]-'0')*round(pow(10,power));
            power++;
            convertToInt(stri.substr(0,stri.size()-1));
        }
return integer;
}
int main()
{
    try
        {
            string Number;
            cout << "Enter Your Number: "<< endl;
            cin>>Number;
            cout <<"Your Number After Converting Is "<<convertToInt(Number)<< endl;
        }
    catch(ExceptionClass::OutRange)
        {
            cout << "\n >> The String Must Have Only Integer Numbers << "<<endl;
        }
    return 0;
}
